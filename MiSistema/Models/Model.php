<?php

include 'Conexion.php';

class Model
{
	private $conexion;
	protected $table;
	protected $columns;
	protected $types;
	
	public function __construct($table,$columns,$types) {
        $this->table= $table;
        $this->columns= $columns;
        $this->types= $types;
        $this->conexion= new Conexion();
    }

	protected function find() {
		$qry = "SELECT * FROM ".$this->table;
		$result=mysqli_query ($this->conexion->con(),$qry);
		return $result;
	}

	protected function findColumns($ncol) {
		$qry = "SELECT ";
		$i=0;
		while($i<sizeof($ncol)){
			$qry .= $this->columns[$ncol[$i]];
			$i++;
			if($i==sizeof($ncol)-1){
				$qry .=", ";
			}
		}
		$qry .= " FROM ".$this->table;
		$result=mysqli_query ($this->conexion->con(),$qry);
		return $result;
	}

	protected function edit($sourceColumns,$columnsValues,$condition) {
		$qry = "UPDATE ".$this->table;
		$qry .= " SET ";
		for ($i=0; $i < sizeof($sourceColumns); $i++) {
			switch ($this->types[$sourceColumns[$i]]) {
				case 'double':
			 	case 'int':
			 		$qry .= $this->columns[$sourceColumns[$i]]."=".$columnsValues[$i];
			 		break;
			 	case 'text':
			 		$qry .= $this->columns[$sourceColumns[$i]]."='".$columnsValues[$i]."'";
			 		break;
			}
			if($i+1==sizeof($sourceColumns)-1){
				$qry .=", ";
			}
		}
		$qry .= " WHERE ".$condition;
		$qry = str_replace(", WHERE ", " WHERE ", $qry);
		echo $qry;
		if($this->conexion->con()->query($qry)===TRUE){
			mysqli_close($this->conexion->con());
			return true;
		}else{
			mysqli_close($this->conexion->con());
			return false;
		}
	}

	protected function addData($columnsValues) {
		$i=0;
		$qry = "INSERT INTO ".$this->table."(";
		foreach ($this->columns as $col) {
	 		$qry .= $col;
			$i++;
			if($i!=sizeof($this->types)){
				$qry .=", "; 
			}
		}
		$qry .= ") VALUES(";
		for ($i=0; $i < sizeof($columnsValues); $i++) { 
			switch ($this->types[$i]) {
				case 'double':
			 	case 'int':
			 		$qry .= $columnsValues[$i];
			 		break;
			 	case 'text':
			 		$qry .= "'".$columnsValues[$i]."'";
			 		break;
		 	}
			if($i+1!=sizeof($columnsValues)){
				$qry .=", "; 
			}
		}
		$qry .= ");";
		echo $qry."<br>";
		if($this->conexion->con()->query($qry)===TRUE){
			mysqli_close($this->conexion->con());
			return true;
		}else{
			mysqli_close($this->conexion->con());
			return false;
		}
	}

	protected function fetch($result){
		$j=0;
		$i=0;
		while ($row=mysqli_fetch_assoc($result)) {
			
			while($i<sizeof($this->columns)){
				$resultado[$j][$this->columns[$i]]=$row[$this->columns[$i]];
				$i++;
			}

			$i=0;
			$j++;
		}
		return $resultado;
	}

	protected function fetchs($result,$ncols){
		$j=0;
		$i=0;
		while ($row=mysqli_fetch_assoc($result)) {
			while($i<sizeof($ncols)){
				$resultado[$j][$this->columns[$ncols[$i]]]=$row[$this->columns[$ncols[$i]]];
				$i++;
			}
			$i=0;
			$j++;
		}
		return $resultado;
	}
}

?>